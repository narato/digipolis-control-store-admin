﻿namespace Digipolis.SurveyComposer.ControlStore.Common.Configurations
{
    public class DAMConfiguration
    {
        public string Url { get; set; }
        public string ApiKey { get; set; }
    }
}
