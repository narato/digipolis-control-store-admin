﻿namespace Digipolis.SurveyComposer.ControlStore.Common.Configurations
{
    public class ControlStoreConfiguration
    {
        public string ControlStoreEndpoint { get; set; }
        public string AuthHeaderValue { get; set; }
    }
}
