﻿namespace Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring
{
    public class SystemStatusComponent
    {
        public string Status { get; set; } // can be "ok", "warning", "error"
        public string Name { get; set; }
        public string Type { get; set; }
        public string Details { get; set; }
        public string ErrorMessage { get; set; }
    }
}
