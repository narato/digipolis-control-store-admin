﻿using System.Collections.Generic;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring
{
    public class SystemStatus
    {
        public string Status { get; set; } // can be "ok", "warning", "error"
        public ICollection<SystemStatusComponent> Components { get; set; }

        public SystemStatus()
        {
            Components = new List<SystemStatusComponent>();
        }
    }
}
