﻿namespace Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring
{
    public class Ping
    {
        public string Status { get; set; }
    }
}
