﻿using Newtonsoft.Json;
using System;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class RuntimeMetadata
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string Version { get; set; }
        public string Wrapper { get; set; }
        public FilesMetadata Files { get; set; }

        [JsonIgnore]
        public ControlMetadata ControlMetadata { get; set; }
        [JsonIgnore]
        public Guid FilesId { get; set; }
    }
}
