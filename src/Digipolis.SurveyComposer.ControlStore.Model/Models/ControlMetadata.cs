﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class ControlMetadata
    {
        public Guid Id { get; set; }
        [Required]
        public RuntimeMetadata RuntimeMetadata { get; set; }
        [Required]
        public DesigntimeMetadata DesigntimeMetadata { get; set; }

        [JsonIgnore]
        public Guid RuntimeMetadataId { get; set; }
        [JsonIgnore]
        public Guid DesigntimeMetadataId { get; set; }
    }
}
