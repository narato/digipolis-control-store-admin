﻿using Newtonsoft.Json;
using System;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class FilePath
    {
        public Guid Id { get; set; }
        public string Path { get; set; }

        [JsonIgnore]
        public Guid FilesMetadataId { get; set; }
        [JsonIgnore]
        public FilesMetadata FilesMetadata { get; set; }
    }
}
