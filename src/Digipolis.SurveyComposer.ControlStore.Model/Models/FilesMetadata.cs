﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class FilesMetadata
    {
        public Guid Id { get; set; }
        public ICollection<FilePath> Code { get; set; }
        public string Styles { get; set; }
        public string Template { get; set; }

        [JsonIgnore]
        public RuntimeMetadata RuntimeMetadata { get; set; }
    }
}
