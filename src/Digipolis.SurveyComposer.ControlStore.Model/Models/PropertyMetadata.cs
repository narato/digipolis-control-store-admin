﻿using Newtonsoft.Json;
using System;

namespace Digipolis.SurveyComposer.ControlStore.Model.Models
{
    public class PropertyMetadata
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public Guid DesigntimeMetadataId { get; set; }
        [JsonIgnore]
        public DesigntimeMetadata DesigntimeMetadata { get; set; }
    }
}
