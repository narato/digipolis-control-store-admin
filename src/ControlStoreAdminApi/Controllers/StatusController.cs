﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.Npm.Models;
using Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring;
using Microsoft.AspNetCore.Mvc;
using Narato.Common.Models;
using System.Threading.Tasks;

namespace ControlStoreAdminApi.Controllers
{
    [Route("api/status/")]
    public class StatusController : Controller
    {
        private readonly IMonitoringService _monitoringService;

        public StatusController(IMonitoringService monitoringService)
        {
            _monitoringService = monitoringService;
        }

        /// <summary>
        /// This is a heartbeat endpoint. If you get a response, you know the engine is up
        /// </summary>
        /// <returns>a ping object, proving that the server is accessible</returns>
        [ProducesResponseType(typeof(Ping), (int)System.Net.HttpStatusCode.OK)]
        [HttpGet("ping")]
        public IActionResult Get()
        {
            var ping = new Ping
            {
                Status = "ok"
            };
            return new OkObjectResult(ping);
        }

        [ProducesResponseType(typeof(SystemStatus), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("monitoring")]
        public async Task<IActionResult> GetMonitoring()
        {
            return new OkObjectResult(await _monitoringService.GetSystemStatus());
        }
    }
}
