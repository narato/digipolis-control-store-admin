﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Threading.Tasks;

namespace ControlStoreAdminApi.Controllers
{
    [Route("api/installs")]
    public class InstallController : Controller
    {
        private readonly IResponseFactory _responseFactory;
        private readonly IInstallManager _installManager;

        public InstallController(IResponseFactory responseFactory, IInstallManager installManager)
        {
            _responseFactory = responseFactory;
            _installManager = installManager;
        }

        /// <summary>
        /// installs a new control
        /// </summary>
        /// <param name="controlName">the name of the control to install</param>
        /// <param name="controlVersion">the version of the control to install</param>
        /// <returns>the installed control</returns>
        [ProducesResponseType(typeof(Response<ControlMetadata>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPost("{controlName}/{controlVersion}")]
        public async Task<IActionResult> InstallControl(string controlName, string controlVersion)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _installManager.InstallControl(controlName, controlVersion), this.GetRequestUri());
        }
    }
}
