﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Narato.Common.ActionFilters;
using Newtonsoft.Json.Serialization;
using Narato.Common.DependencyInjection;
using NLog.Extensions.Logging;
using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.Domain.Mappers;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Domain.DAM;
using Microsoft.Extensions.Options;
using Digipolis.SurveyComposer.ControlStore.Common.Configurations;
using System.Net.Http;
using System.Net.Http.Headers;
using Digipolis.SurveyComposer.ControlStore.Domain.Npm;
using Digipolis.SurveyComposer.ControlStore.Domain.Managers;
using Digipolis.SurveyComposer.ControlStore.Domain.Clients;
using Swashbuckle.Swagger.Model;
using System.Collections.Generic;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Digipolis.SurveyComposer.ControlStore.Domain.Monitoring;

namespace ControlStoreAdminApi
{
    public class Startup
    {
        private MapperConfiguration _mapperConfiguration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("config.json")
                .AddJsonFile("config.json.local", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            _mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfileConfiguration());
            });
            _mapperConfiguration.AssertConfigurationIsValid();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ControlStoreConfiguration>(Configuration.GetSection("ControlStoreConfiguration"));
            services.Configure<NpmConfiguration>(Configuration.GetSection("NpmConfiguration"));
            services.Configure<DAMConfiguration>(Configuration.GetSection("DAMConfiguration"));
            // Add framework services.
            services.AddMvc(
                //Add this filter globally so every request runs this filter to recored execution time
                config =>
                {
                    config.Filters.Add(new ExecutionTimingFilter());
                    config.Filters.Add(new ModelValidationFilter());
                })
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
                }
            );

            services.AddCors();

            services.AddTransient<IInstallManager, InstallManager>();
            services.AddTransient<IMonitoringService, MonitoringService>();

            services.AddSingleton<IControlStoreClient, ControlStoreClient>(c =>
            {
                var config = c.GetService<IOptions<ControlStoreConfiguration>>();
                var client = GetBasicHttpClient(config.Value.ControlStoreEndpoint);
                client.DefaultRequestHeaders.Add("X-Custom-Auth", config.Value.AuthHeaderValue);
                return new ControlStoreClient(client);
            });

            services.AddSingleton<IDAMManager, DAMManager>(c =>
            {
                var config = c.GetService<IOptions<DAMConfiguration>>();
                var client = GetBasicHttpClient(config.Value.Url);
                if (!client.DefaultRequestHeaders.Contains("apikey"))
                {
                    client.DefaultRequestHeaders.Add("apikey", config.Value.ApiKey);
                }
                return new DAMManager(client);
            });

            services.AddSingleton<INpmHandler, NpmHandler>(c => {
                var config = c.GetService<IOptions<NpmConfiguration>>();
                var client = GetBasicHttpClient(config.Value.Registry);
                return new NpmHandler(client);
            });

            services.AddSingleton(sp => _mapperConfiguration.CreateMapper());

            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "ControlStore Admin API",
                    Version = "v1",
                    Title = "ControlStore"
                });
                //options.OperationFilter<ProducesConsumesFilter>();

                var xmlPaths = GetXmlCommentsPaths();
                foreach (var entry in xmlPaths)
                {
                    try
                    {
                        options.IncludeXmlComments(entry);
                    }
                    catch (Exception e)
                    {
                    }
                }
            });

            services.AddNaratoCommon();
        }

        private HttpClient GetBasicHttpClient(string url)
        {
            var client = new HttpClient() { BaseAddress = new Uri(url) };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            loggerFactory.AddNLog();

            env.ConfigureNLog("nlog.config");

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            app.UseMvc();
        }

        private List<string> GetXmlCommentsPaths()
        {
            var app = PlatformServices.Default.Application;
            var files = new List<string>()
                        {
                            "ControlStoreAdminApi.xml"
                        };

            List<string> paths = new List<string>();
            foreach (var file in files)
            {
                paths.Add(Path.Combine(app.ApplicationBasePath, file));
            }

            return paths;
        }
    }
}
