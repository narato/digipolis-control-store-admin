﻿using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Newtonsoft.Json.Linq;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Mappers
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        {
            CreateMap<JObject, ControlMetadata>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RuntimeMetadataId, opt => opt.Ignore())
                .ForMember(dest => dest.DesigntimeMetadataId, opt => opt.Ignore())
                .ForMember(dest => dest.RuntimeMetadata, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.DesigntimeMetadata, opt => opt.MapFrom(src => src));


            CreateMap<JObject, RuntimeMetadata>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.FilesId, opt => opt.Ignore())
                .ForMember(dest => dest.ControlMetadata, opt => opt.Ignore())
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src["name"]))
                .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src["version"]))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src["description"]))
                .ForMember(dest => dest.Wrapper, opt => opt.MapFrom(src => src["runtime_metadata"]["wrapper"]))
                .ForMember(dest => dest.Files, opt => opt.MapFrom(src => src["runtime_metadata"]["files"]));

            CreateMap<JToken, FilesMetadata>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RuntimeMetadata, opt => opt.Ignore())
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src["code"]))
                .ForMember(dest => dest.Styles, opt => opt.MapFrom(src => src["styles"]))
                .ForMember(dest => dest.Template, opt => opt.MapFrom(src => src["template"]));

            CreateMap<JToken, FilePath>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.FilesMetadataId, opt => opt.Ignore())
                .ForMember(dest => dest.FilesMetadata, opt => opt.Ignore())
                .ForMember(dest => dest.Path, opt => opt.MapFrom(src => src));

            CreateMap<JObject, DesigntimeMetadata>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ControlMetadata, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src["name"]))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src["description"]))
                .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src["version"]))
                .ForMember(dest => dest.Logo, opt => opt.MapFrom(src => src["design_metadata"]["logo"]))
                .ForMember(dest => dest.Properties, opt => opt.MapFrom(src => src["design_metadata"]["properties"]));

            CreateMap<JToken, PropertyMetadata>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.DesigntimeMetadata, opt => opt.Ignore())
                .ForMember(dest => dest.DesigntimeMetadataId, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src["name"]))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src["type"]))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src["description"]));
        }
    }
}
