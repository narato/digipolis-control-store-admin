﻿using Digipolis.SurveyComposer.ControlStore.Domain.DAM.Models;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System;
using System.Net;

namespace Digipolis.SurveyComposer.ControlStore.Domain.DAM
{
    public class DAMManager : IDAMManager
    {
        private const string USER_ID = "1"; // no idea what this is for... but there is NO documentation, and this works
        private readonly HttpClient _client;

        public DAMManager(HttpClient httpClient)
        {
            _client = httpClient;
        }

        public async Task<DAMAsset> UploadFile(Stream fileStream, string fileName)
        {
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(USER_ID), "userId");

            var fileContent = new StreamContent(fileStream);

            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                FileName = fileName,
                Name = "file"
            };
            content.Add(fileContent);

            var request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + "api/mediafiles");
            request.Content = content;

            using (var response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
            {
                response.EnsureSuccessStatusCode();
                var responseString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DAMAsset>(responseString);
            }
        }

        public async Task<bool> IsDAMAccessible()
        {
            using (var response = await _client.GetAsync("admin/version"))
            {
                var responseString = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                return false;
            }
        }

    }
}
