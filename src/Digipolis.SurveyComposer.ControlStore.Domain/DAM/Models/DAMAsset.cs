﻿using System.Collections.Generic;

namespace Digipolis.SurveyComposer.ControlStore.Domain.DAM.Models
{
    public class DAMAsset
    {
        public string FileName { get; set; }
        public string AssetId { get; set; }
        public IEnumerable<AssetLink> Links { get; set; }
        public string MediafileId { get; set; }
        public bool ThumbnailGenerated { get; set; }

    }
}
