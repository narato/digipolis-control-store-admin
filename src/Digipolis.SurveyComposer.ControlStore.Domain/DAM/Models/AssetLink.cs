﻿namespace Digipolis.SurveyComposer.ControlStore.Domain.DAM.Models
{
    public class AssetLink
    {
        public string Rel { get; set; }
        public string Href { get; set; }
    }
}
