﻿namespace Digipolis.SurveyComposer.ControlStore.Domain.Npm.Models
{
    public class NpmErrorResponse
    {
        public string Error { get; set; }
    }
}
