﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Npm
{
    public class NpmHandler : INpmHandler
    {
        private readonly HttpClient _client;

        public NpmHandler(HttpClient httpClient)
        {
            _client = httpClient;
        }

        public async Task<JObject> GetPackageJsonOfControlByNameAndVersion(string name, string version)
        {
            using (var response = await _client.GetAsync($"{name}/{version}"))
            {
                var responseString = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return JObject.Parse(responseString);
                } else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    var errorResponse = JObject.Parse(responseString);
                    throw new EntityNotFoundException("could not get package.json of control. Error: " + errorResponse.Value<string>("error"));
                } else
                {
                    throw new ExceptionWithFeedback(FeedbackItem.CreateErrorFeedbackItem($"Error when getting package.json of control with name {name} and version {version}. got statuscode {response.StatusCode}"));
                }
            }
        }

        public async Task<bool> IsNpmRegistryAccessible()
        {
            using (var response = await _client.GetAsync(""))
            {
                var responseString = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
