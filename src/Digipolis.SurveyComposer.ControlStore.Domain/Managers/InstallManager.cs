﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using SharpCompress.Readers;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Digipolis.SurveyComposer.ControlStore.Domain.DAM.Models;
using System.Linq;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Managers
{
    public class InstallManager : IInstallManager
    {
        private readonly INpmHandler _npmHandler;
        private readonly IDAMManager _damManager;
        private readonly IControlStoreClient _controlStoreClient;
        private readonly IMapper _mapper;

        public InstallManager(INpmHandler npmHandler, IDAMManager damManager, IControlStoreClient controlStoreClient, IMapper mapper)
        {
            _npmHandler = npmHandler;
            _damManager = damManager;
            _controlStoreClient = controlStoreClient;
            _mapper = mapper;
        }

        public async Task<ControlMetadata> InstallControl(string controlName, string controlVersion)
        {
            var existingControl = await _controlStoreClient.FindControlMetadataByNameAndVersionAsync(controlName, controlVersion);
            if (existingControl != null)
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("This control with this version already exists. Doing nothing."));
            }

            var packageJson = await _npmHandler.GetPackageJsonOfControlByNameAndVersion(controlName, controlVersion);
            var tarballUrl = packageJson["dist"].Value<string>("tarball");

            if (packageJson["runtime_metadata"] == null)
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("This is not a valid control package (requires a runtime_metadata property)"));
            }

            // TODO: move the packagejson altering code to its own service and inject here
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));

            var styleFile = packageJson["runtime_metadata"]?["files"]?.Value<string>("styles").ToLower();
            var templateFile = packageJson["runtime_metadata"]?["files"]?.Value<string>("template").ToLower();

            var codeFiles = new List<string>();
            var codeFilesOnDAM = new List<string>();
            JArray rawCodeFiles = (JArray)packageJson["runtime_metadata"]?["files"]?["code"];
            if (rawCodeFiles != null)
            {
                foreach (var codeFile in rawCodeFiles)
                {
                    codeFiles.Add(codeFile.ToString().ToLower());
                }
            }

            if (styleFile == null && templateFile == null && codeFiles.Count == 0)
            {
                throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("This is not a valid control package (has 0 files defined in the metadata)"));
            }

            using (Stream stream = await client.GetStreamAsync(tarballUrl))
            {
                var reader = ReaderFactory.Open(stream);
                while (reader.MoveToNextEntry())
                {
                    if (!reader.Entry.IsDirectory)
                    {
                        var fileName = reader.Entry.Key.Substring(reader.Entry.Key.LastIndexOf("/") + 1).ToLower();
                        if (templateFile.Equals(fileName) || styleFile.Equals(fileName) || codeFiles.Contains(fileName))
                        {
                            DAMAsset damAsset;
                            using (var memoryStream = new MemoryStream())
                            {
                                reader.WriteEntryTo(memoryStream);

                                memoryStream.Position = 0; // we just wrote to the stream, now we have to playback it
                                damAsset = await _damManager.UploadFile(memoryStream, fileName);
                            }

                            // use the /media/ path instead of /download/ because CORS headers are set on this URL
                            var path = damAsset.Links.First().Href.Replace("/download/", "/media/");

                            if (templateFile.Equals(fileName))
                            {
                                packageJson["runtime_metadata"]["files"]["template"] = path;
                            }
                            if (styleFile.Equals(fileName))
                            {
                                packageJson["runtime_metadata"]["files"]["template"] = path;
                            }
                            if (codeFiles.Contains(fileName))
                            {
                                codeFilesOnDAM.Add(path);
                            }
                        }
                    }
                }
            }

            var codeJArray = new JArray();
            foreach (var codeFileOnDAM in codeFilesOnDAM)
            {
                codeJArray.Add(codeFileOnDAM);
            }

            packageJson["runtime_metadata"]["files"]["code"] = codeJArray;

            return await _controlStoreClient.InsertControlMetadataAsync(_mapper.Map<ControlMetadata>(packageJson));
        }
    }
}
