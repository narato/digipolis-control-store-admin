﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using System;
using System.Threading.Tasks;
using Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Monitoring
{
    public class MonitoringService : IMonitoringService
    {
        private readonly IControlStoreClient _controlStoreClient;
        private readonly INpmHandler _npmHandler;
        private readonly IDAMManager _damManager;

        public MonitoringService(IControlStoreClient controlStoreClient, INpmHandler npmHandler, IDAMManager damManager)
        {
            _controlStoreClient = controlStoreClient;
            _npmHandler = npmHandler;
            _damManager = damManager;
        }

        public async Task<SystemStatus> GetSystemStatus()
        {
            var returnValue = new SystemStatus();
            var status = "ok";

            // controlstore API
            try
            {
                var ping = await _controlStoreClient.GetPing();
                if (ping.Status == "ok")
                {
                    returnValue.Components.Add(new SystemStatusComponent
                    {
                        Status = "ok",
                        Name = "ControlStore API",
                        Type = "API",
                        Details = "Ping was successful",
                        ErrorMessage = ""
                    });
                } else
                {
                    returnValue.Components.Add(new SystemStatusComponent
                    {
                        Status = "warning",
                        Name = "ControlStore API",
                        Type = "API",
                        Details = "got back a succesful statuscode, but we didn't get 'status ok'",
                        ErrorMessage = ""
                    });
                    if (status != "error")
                        status = "warning";
                }
            } catch (Exception ex)
            {
                returnValue.Components.Add(new SystemStatusComponent
                {
                    Status = "error",
                    Name = "ControlStore API",
                    Type = "API",
                    Details = "Ping failed",
                    ErrorMessage = ex.Message
                });
                status = "error";
            }

            // NPM registry
            if (await _npmHandler.IsNpmRegistryAccessible())
            {
                returnValue.Components.Add(new SystemStatusComponent
                {
                    Status = "ok",
                    Name = "Npm registry",
                    Type = "NPM",
                    Details = "Npm registry is up",
                    ErrorMessage = ""
                });
            } else
            {
                returnValue.Components.Add(new SystemStatusComponent
                {
                    Status = "error",
                    Name = "Npm registry",
                    Type = "NPM",
                    Details = "Npm registry is down",
                    ErrorMessage = "Npm registry is down, or is inaccessible"
                });
                status = "error";
            }

            // DAM api

            if (await _damManager.IsDAMAccessible())
            {
                returnValue.Components.Add(new SystemStatusComponent
                {
                    Status = "ok",
                    Name = "DAM",
                    Type = "API",
                    Details = "DAM is up",
                    ErrorMessage = ""
                });
            } else
            {
                returnValue.Components.Add(new SystemStatusComponent
                {
                    Status = "error",
                    Name = "DAM",
                    Type = "API",
                    Details = "DAM is down",
                    ErrorMessage = "DAM is down, or is inaccessible"
                });
                status = "error";
            }

            returnValue.Status = status;
            return returnValue;
        }
    }
}
