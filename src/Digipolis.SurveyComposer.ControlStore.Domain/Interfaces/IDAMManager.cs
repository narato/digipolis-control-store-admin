﻿using Digipolis.SurveyComposer.ControlStore.Domain.DAM.Models;
using System.IO;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IDAMManager
    {
        Task<DAMAsset> UploadFile(Stream fileStream, string fileName);
        Task<bool> IsDAMAccessible();
    }
}
