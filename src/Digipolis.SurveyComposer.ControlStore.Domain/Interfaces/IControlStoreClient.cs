﻿using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IControlStoreClient
    {
        Task<ControlMetadata> InsertControlMetadataAsync(ControlMetadata control);
        Task<ControlMetadata> FindControlMetadataByNameAndVersionAsync(string name, string version);

        Task<Ping> GetPing();
    }
}
