﻿using Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IMonitoringService
    {
        Task<SystemStatus> GetSystemStatus();
    }
}
