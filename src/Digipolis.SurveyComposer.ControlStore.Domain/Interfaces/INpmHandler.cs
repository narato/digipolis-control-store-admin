﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface INpmHandler
    {
        Task<JObject> GetPackageJsonOfControlByNameAndVersion(string name, string version);

        Task<bool> IsNpmRegistryAccessible();
    }
}
