﻿using Digipolis.SurveyComposer.ControlStore.Model.Models;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Interfaces
{
    public interface IInstallManager
    {
        Task<ControlMetadata> InstallControl(string controlName, string controlVersion);
    }
}
