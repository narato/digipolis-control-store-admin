﻿using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Clients.ResponseExtensions
{
    public static class HttpResponseMessageExtension
    {
        public async static Task<T> HandleResponseAsync<T>(this HttpResponseMessage response, string badRequestMessage)
        {
            var responseString = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
            {
                var objectResponse = JsonConvert.DeserializeObject<Response<T>>(responseString);
                return objectResponse.Data;
            }

            MapUnsuccessfulStatusCode(response, responseString, badRequestMessage);

            return default(T);
        }

        private static void MapUnsuccessfulStatusCode(HttpResponseMessage response, string responseString, string badRequestMessage)
        {
            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                var dataTypeResponse = JsonConvert.DeserializeObject<Response>(responseString);
                var feedbackList = dataTypeResponse.Feedback;
                feedbackList.Add(new FeedbackItem() { Description = badRequestMessage });
                throw new ExceptionWithFeedback(feedbackList);
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                var dataTypeResponse = JsonConvert.DeserializeObject<Response>(responseString);
                if (dataTypeResponse.Feedback.Count > 0)
                {
                    throw new EntityNotFoundException(dataTypeResponse.Feedback[0].Description);
                }
                throw new EntityNotFoundException();
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                var dataTypeResponse = JsonConvert.DeserializeObject<Response>(responseString);
                if (dataTypeResponse.Feedback.Count > 0)
                {
                    throw new ExceptionWithFeedback(dataTypeResponse.Feedback);
                }
                throw new ExceptionWithFeedback(new FeedbackItem() { Type = FeedbackType.Error, Description = "An error occured while talking to the DataStore Engine" });
            }
        }
    }
}
