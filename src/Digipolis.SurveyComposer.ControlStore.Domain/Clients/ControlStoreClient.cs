﻿using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using System.Threading.Tasks;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using System.Net.Http;
using Narato.Common;
using System.Net.Http.Headers;
using Digipolis.SurveyComposer.ControlStore.Domain.Clients.ResponseExtensions;
using Narato.Common.Exceptions;
using Digipolis.SurveyComposer.ControlStore.Model.Models.Monitoring;
using System;
using Newtonsoft.Json;

namespace Digipolis.SurveyComposer.ControlStore.Domain.Clients
{
    public class ControlStoreClient : IControlStoreClient
    {
        private readonly HttpClient _client;

        public ControlStoreClient(HttpClient httpClient)
        {
            _client = httpClient;
        }

        public async Task<ControlMetadata> InsertControlMetadataAsync(ControlMetadata control)
        {
            var payloadAsJson = control.ToJson();

            var content = new StringContent(payloadAsJson);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var request = new HttpRequestMessage(HttpMethod.Post, $"controls");
            request.Content = content;

            using (var response = await _client.SendAsync(request))
            {
                return await response.HandleResponseAsync<ControlMetadata>($"The control couldn't be inserted.");
            }
        }

        public async Task<ControlMetadata> FindControlMetadataByNameAndVersionAsync(string name, string version)
        {
            using (var response = await _client.GetAsync($"controls/{name}/versions/{version}"))
            {
                try
                {
                    return await response.HandleResponseAsync<ControlMetadata>($"The control couldn't be retrieved.");
                } catch (EntityNotFoundException)
                {
                    // couldn't be found, and this is a FindBy method, so return null
                    return null;
                }
            }
        }

        public async Task<Ping> GetPing()
        {
            using (var response = await _client.GetAsync($"status/ping"))
            {
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Ping>(responseString);
            }
        }
    }
}
