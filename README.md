# Control Store
De Control Store is een CDN dat Custom Controls aanbiedt die gebruikt kunnen worden door de Form & Survey Renderer. Custom Controls worden aangeleverd als NPM packages. Deze worden manueel (in een latere fase automatisch) gedeployed in de Control Store waarna ze onmiddellijk gebruikt kunnen worden in templates en gevisualiseerd kunnen worden door de Form Renderer.

In vergelijking met een gewone file server biedt de Control Store een REST API aan waarin o.a. de metadata en een specifieke versie van een control kan opgehaald worden m.b.v. semantic versioning.

## Custom Controls NPM package

Een Custom Control wordt beschreven door metadata in de `package.json` van zijn NPM package.

De volgende attributen moeten beschikbaar zijn in het top-level van de `package.json`:

| Field | Verplicht | Beschrijving |
--------|-----------|------------- |
| name | x | System name van de control. Deze naam zal gebruikt worden om naar de control te refereren. |
| description | x | Human-readable name van de control |
| version | x | Versie van de control (volgens semantic versioning) |

Voorbeeld `package.json`:
```javascript
{
  ...
  "name": "text",
  "version": "1.0.0",
  "description": "Text control"
  ...
}
```

De overige metadata van de control wordt beschreven in custom elementen in `package.json`.
Momenteel zijn er 2 soorten metadata:
* runtime metadata: metadata om de control te visualizeren in een formulier (vb. door de Form Renderer)
* design metadata: metadata om de control te gebruiken en configureren in een design tool waarmee form templates gemaakt kunnen worden

### Runtime metadata

De runtime metadata bevat dezelfde content als de control.json. De metadata wordt gebruikt om de control te visualizeren in een formulier.

| Field | Verplicht | Beschrijving |
--------|-----------|--------------|
| wrapper | x | Referentie (by name) naar de wrapper waarin de control gezet wordt |
| viewWrapper | | Referentie (by name) naar de wrapper waarin de control gezet wordt in read-only mode |
| files | x | Alle bestanden (template, styling, gedrag, ...) gerelateerd aan de control. De filenames zijn relatief t.o.v. de root folder van de control |
| files.code | x | Array van javascript filenames die het gedrag van de control bevatten |
| files.styles | x | Filename van CSS bestand dat de control styling bevat |
| files.template | x | Filename van het HTML template bestand van de control |
| files.viewTemplate | | Filename van de HTML template van de control in read-only mode |

Voorbeeld `package.json`:
```javascript
{
  ...
  "runtime_metadata": {
    "wrapper": "default",
    "files": {
      "code": [
        "text.js"
      ],
      "styles": "",
      "template": "text.html"
    }
  }
  ...
}
```

#### files.code

Als je Angular code wilt toevoegen die gekoppeld dient te worden aan de control, volg dan volgende structuur:

```javascript
(function (ng) {
    'use strict';
    var controls = ng.module('formRenderer.controls');
    controls.service('frFieldTypeService-<control-type>', [
        '$document',
        function($document) {
            return {
                link: function (scope, element, attrs) {
                    function initialize() {
                        //...
                    }

                    // private functions...

                    // extend control scope
                    scope.someMethod = function () {
                        //...
                    };

                    //...

                    initialize();

                }
            };
        }
    ]);
})(window.angular);
```

De naam van de Angular service dient opgebouwd te worden uit een prefix 'frFieldTypeService-', gevolgd door de naam van de control, zoals die opgegeven is in de package.json.
De Angular service zelf geeft een object terug met attribuut 'link', wat op zich een functie is de code bevat om aan de control toe te voegen.

### Design metadata
De design metadata wordt gebruikt om de control weer te geven en te configureren in een tool die gebruikt wordt om formulier op te stellen.

| Field | Verplicht | Beschrijving |
--------|-----------|--------------|
| logo | | Filename van het logo van de control (relatief t.o.v. de root folder van de control) |
| properties | | Array van objecten die de verschillende eigenschappen van de control beschrijven die door de designer geconfigureerd kunnen worden. Vb. help text, placeholder text, key-value pairs van een dropdown, ... |
| properties.[].name | | Naam van de eigenschap |
| properties.[].type | | Datatype van de eigenschap |
| properties.[].description | | Beschrijving van de eigenschap |

Voorbeeld `package.json`:
```javascript
{
  ...
  "design_metadata": {
    "logo": "",
    "properties": [
      {
        "name": "placeholder",
        "type": "text",
        "description": "Placeholder tekst voor het tekstveld"
      }
    ]
  }
  ...
}
```

Voorbeeld
Een `package.json` van een text control zou er bijvoorbeeld als volgt kunnen uitzien:
```javascript
{
   "name": "text",
   "version": "1.0.0",
   "description": "Text control",
   "runtime_metadata": {
     "wrapper": "default",
     "files": {
       "code": [
         "text.js"
       ],
       "styles": "",
       "template": "text.html"
     }
  },
  "design_metadata": {
    "logo": "",
    "properties": [
      {
        "name": "placeholder",
        "type": "text",
        "description": "Placeholder tekst voor het tekstveld"
      }
    ]
  }
}
```

## Control Store gebruiken in de Form Renderer
Om de Control Store te gebruiken volstaat het om de `control-cdn-url` eigenschap in te stellen op het `fr-form` directive. De naam van de control in de Form JSON moet dezelfde zijn als de naam van het Custom Control NPM package. Na de naam kan ook d.m.v. `#` een (semantische) versie gespecifieerd worden (vb. `text`, `text#1`, `text#1.0` of `text#1.0.0`). Indien er geen versie gespecifieerd wordt, zal automatisch de laatste versie van de control opgehaald worden.

Vb.
```javascript
...
"fields": [
  {
    "name": "firstname",
    "spec": {
      "attributes": {
        "type": "text#1.0",
        "placeholder": "John"
      },
      "options": {
        "label": "Firstname",
        "displayLabel": true,
        "layout": {
          "fieldLayout": "full",
          "fieldClass": "span-full tablet--span-6-1 desktop--span-6-1"
        }
      }
    },
      "editable": true,
      "editMode": true
    }
  }
]
...
```

Bij het renderen van een control zal de Form Renderer de control eerst lokaal proberen op te halen. Indien de control lokaal niet beschikbaar is, wordt hij opgehaald uit de Control Store via de ingestelde `control-cdn-url`.

Vb.
```html
<fr-form
    schema="form.schema"
    on-navigate="form.onNavigate"
    on-submit="form.onSubmit"
    control-cdn-url="http://controlstore.azurewebsites.net/api">
</fr-form>
```