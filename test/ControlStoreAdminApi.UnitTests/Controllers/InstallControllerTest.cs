﻿using ControlStoreAdminApi.Controllers;
using Digipolis.SurveyComposer.ControlStore.Domain.Interfaces;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreAdminApi.UnitTests.Controllers
{
    public class InstallControllerTest
    {
        [Fact]
        public void GetAllReturnsAnActionResult()
        {
            var installManagerMock = new Mock<IInstallManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<ControlMetadata>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var controller = new InstallController(factoryMock.Object, installManagerMock.Object);

            IActionResult actionResult = controller.InstallControl("meep", "moop").Result;

            Assert.NotNull(actionResult);
        }
    }
}
