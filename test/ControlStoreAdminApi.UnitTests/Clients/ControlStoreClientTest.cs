﻿using ControlStoreAdminApi.UnitTests.Stubs;
using Digipolis.SurveyComposer.ControlStore.Domain.Clients;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Moq;
using Narato.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ControlStoreAdminApi.UnitTests.Clients
{
    public class ControlStoreClientTest
    {
        [Fact]
        public void FindControlMetadataByNameAndVersionTest()
        {
            var control = new ControlMetadata
            {
                Id = Guid.NewGuid()
            };

            var controlResponse = new Response<ControlMetadata>
            {
                Data = control
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            Expression<Func<HttpRequestMessage, bool>> getNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Get &&
                 msg.RequestUri.ToString() == $"http://test/controls/name/versions/version";

            var getHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            getHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(controlResponse));

            msgHandler.Setup(t => t.Send(It.Is(getNamespaceRequestMessageArgument)))
                .Returns(getHttpResponseMessage);

            var client = new ControlStoreClient(httpClient);

            var returnedControl = client.FindControlMetadataByNameAndVersionAsync("name", "version").Result;

            Assert.Equal(control.Id, returnedControl.Id);
        }

        [Fact]
        public void FindControlMetadataByNameAndVersionReturnsNullWhenNotFoundTest()
        {
            var controlResponse = new Response
            {
                Feedback = new List<FeedbackItem>()
                {
                    FeedbackItem.CreateErrorFeedbackItem("blabla not found blabla")
                }
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            Expression<Func<HttpRequestMessage, bool>> getNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Get &&
                 msg.RequestUri.ToString() == $"http://test/controls/name/versions/version";

            var getHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.NotFound);
            getHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(controlResponse));

            msgHandler.Setup(t => t.Send(It.Is(getNamespaceRequestMessageArgument)))
                .Returns(getHttpResponseMessage);

            var client = new ControlStoreClient(httpClient);

            var returnedControl = client.FindControlMetadataByNameAndVersionAsync("name", "version").Result;

            Assert.Equal(null, returnedControl);
        }

        [Fact]
        public void InsertControlMetadataTest()
        {
            var control = new ControlMetadata
            {
                Id = Guid.NewGuid()
            };

            var controlResponse = new Response<ControlMetadata>
            {
                Data = control
            };

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/");

            Expression<Func<HttpRequestMessage, bool>> postNamespaceRequestMessageArgument = msg =>
                 msg.Method == HttpMethod.Post &&
                 msg.RequestUri.ToString() == $"http://test/controls";

            var postHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            postHttpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(controlResponse));

            msgHandler.Setup(t => t.Send(It.Is(postNamespaceRequestMessageArgument)))
                .Returns(postHttpResponseMessage);

            var client = new ControlStoreClient(httpClient);

            var returnedControl = client.InsertControlMetadataAsync(control).Result;

            Assert.Equal(control.Id, returnedControl.Id);
        }
    }
}
