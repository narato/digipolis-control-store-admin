﻿using AutoMapper;
using Digipolis.SurveyComposer.ControlStore.Domain.Mappers;
using Digipolis.SurveyComposer.ControlStore.Model.Models;
using Newtonsoft.Json.Linq;
using System.Linq;
using Xunit;

namespace ControlStoreAdminApi.UnitTests.Mappers
{
    public class AutoMapperProfileConfigurationTest
    {
        [Fact]
        public void AutoMapperValidConfigurationTest()
        {
            Mapper.Initialize(m => m.AddProfile<AutoMapperProfileConfiguration>());
            Mapper.AssertConfigurationIsValid();
        }

        [Fact]
        public void MapPackageJsonJObjectToControlMetadataTest()
        {
            Mapper.Initialize(m => m.AddProfile<AutoMapperProfileConfiguration>());

            var rawJsonObject = JObject.Parse(GetTestPackageJson());

            var result = Mapper.Map<ControlMetadata>(rawJsonObject);

            Assert.Equal(result.DesigntimeMetadata.Description, "Text Control");
            Assert.Equal(result.DesigntimeMetadata.Logo, "testlogo");
            Assert.Equal(result.DesigntimeMetadata.Name, "text");
            Assert.Equal(result.DesigntimeMetadata.Version, "1.0.0");

            Assert.Equal(result.RuntimeMetadata.Version, "1.0.0");
            Assert.Equal(result.RuntimeMetadata.Key, "text");
            Assert.Equal(result.RuntimeMetadata.Name, "Text Control");
            Assert.Equal(result.RuntimeMetadata.Wrapper, "default");
            Assert.Equal(result.RuntimeMetadata.Files.Styles, "styles.css");
            Assert.Equal(result.RuntimeMetadata.Files.Template, "text.html");

            Assert.Equal(result.DesigntimeMetadata.Properties.Count(), 1);
            Assert.Equal(result.DesigntimeMetadata.Properties.First().Name, "textbox");
            Assert.Equal(result.DesigntimeMetadata.Properties.First().Type, "text");
            Assert.Equal(result.DesigntimeMetadata.Properties.First().Description, "the textbox");

            Assert.Equal(result.RuntimeMetadata.Files.Code.Count(), 1);
            Assert.Equal(result.RuntimeMetadata.Files.Code.First().Path, "text.js");


        }

        private string GetTestPackageJson()
        {
            return @"{
              ""name"": ""text"",
              ""version"": ""1.0.0"",
              ""description"": ""Text Control"",
              ""author"": """",
              ""license"": ""ISC"",
              ""runtime_metadata"": {
                ""wrapper"": ""default"",
                ""files"": {
                  ""code"": [
                    ""text.js""
                  ],
                  ""styles"": ""styles.css"",
                  ""template"": ""text.html""
                }
              },
               ""design_metadata"": {
                ""logo"": ""testlogo"",
                ""properties"": [
                  {
                    ""name"": ""textbox"",
                    ""type"": ""text"",
                    ""description"": ""the textbox""
                  }
                ]
              }
            }
            ";
        }
    }
}
